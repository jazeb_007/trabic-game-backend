
module.exports = async function () {
  try {
    const io = require('./websocket').getio();
    const connected = require('./websocket').getConnectedIds();

    console.log('Fetching coins prices');


    let axios = require("axios");
    let PricesData = require('./schema/PricesModel');

    let gecko_url = 'https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=200&page=1&sparkline=true&price_change_percentage=1h%2C24h%2C7d%2C14d';

    // await PricesData.deleteMany({}); return

    const { data } = await axios.get(gecko_url);

    let db_currencies = await PricesData.find({});



    if (db_currencies.length == 0) await PricesData.insertMany(data);

    let changed_currencies = [];

    for (let db_currency of db_currencies) { // currencies comming from DB

      let { id, symbol, current_price, _id } = db_currency;

      let to_compare = data.filter(c => c.id == id && c.symbol == symbol)[0];

      if (to_compare) {

        let old_price = current_price;
        let new_price = to_compare.current_price;

        console.log(require('./websocket').getConnectedIds());

        console.log({ old_price, new_price }, new_price != old_price, 'value changed for: ' + to_compare.symbol);

        

        if (new_price != old_price) {

          changed_currencies.push(to_compare);

          await PricesData.updateOne({ _id }, { $set: to_compare });

        }

        console.log('Currencies changed: ', changed_currencies.length);

      }
    }
    if (changed_currencies.length > 0) {
      let obj = JSON.stringify({ currencies: changed_currencies });
      for (let conn of connected) {
        console.log('sending to ', conn);
        io.to(conn).emit('currencies', obj);
      }
    }
    changed_currencies.length = 0;
  } catch (error) {
    console.log(error);
    return error;
  }
}







    // console.log("------ db_currencies START ------ ");
    // console.log(db_currencies)
    // console.log("------ db_currencies END ------ ");

    // console.log("------ NEW DATA START ------ ");
    // console.log(data)
    // console.log("------ NEW DATA END ------ ");






// io.send(JSON.stringify({name:'Jazeb', status:'Developer'}));

        // for(let conn of connected) {
        //   console.log('sending to ', conn);
        //   io.to(conn).emit('currencies', obj);
        // }





/**
 *
 * let data  = [
    {
      sparkline_in_7d: { price: [Array] },
      _id: "619397fd1d8a8523433858f9",
      id: 'bitcoin',
      symbol: 'btc',
      name: 'Bitcoin',
      image: 'https://assets.coingecko.com/coins/images/1/large/bitcoin.png?1547033579',
      current_price: 60566,
      market_cap: 1139793436137,
      market_cap_rank: 1,
      fully_diluted_valuation: 1268127182346,
      total_volume: 48104702921,
      high_24h: 66023,
      low_24h: 59150,
      price_change_24h: -5323.525685636881,
      price_change_percentage_24h: -8.07952,
      market_cap_change_24h: -103836852742.60278,
      market_cap_change_percentage_24h: -8.3495,
      circulating_supply: 18874812,
      total_supply: 21000000,
      max_supply: 21000000,
      ath: 69045,
      ath_change_percentage: -12.40628,
      ath_date: "2021-11-10T14:24:11.849Z",
      atl: 67.81,
      atl_change_percentage: 89090.04738,
      atl_date: "2013-07-06T00:00:00.000Z",
      last_updated: "2021-11-16T11:36:15.818Z",
      price_change_percentage_14d_in_currency: -0.9100769315453079,
      price_change_percentage_1h_in_currency: 1.5444275261144536,
      price_change_percentage_24h_in_currency: -8.079523910051458,
      price_change_percentage_7d_in_currency: -10.428499354599856,
      __v: 0
    },
    {
      sparkline_in_7d: { price: [Array] },
      _id: "619397fd1d8a8523433858fa",
      id: 'ethereum',
      symbol: 'eth',
      name: 'Ethereum',
      image: 'https://assets.coingecko.com/coins/images/279/large/ethereum.png?1595348880',
      current_price: 4269.56,
      market_cap: 504161196605,
      market_cap_rank: 2,
      fully_diluted_valuation: null,
      total_volume: 28382123656,
      high_24h: 4770.73,
      low_24h: 4160.79,
      price_change_24h: -461.916486166209,
      price_change_percentage_24h: -9.76262,
      market_cap_change_24h: -56824492831.49902,
      market_cap_change_percentage_24h: -10.1294,
      circulating_supply: 118363822.624,
      total_supply: null,
      max_supply: null,
      ath: 4878.26,
      ath_change_percentage: -12.51192,
      ath_date: "2021-11-10T14:24:19.604Z",
      atl: 0.432979,
      atl_change_percentage: 985605.53303,
      atl_date: "2015-10-20T00:00:00.000Z",
      last_updated: "2021-11-16T11:36:09.825Z",
      price_change_percentage_14d_in_currency: -1.408347993313624,
      price_change_percentage_1h_in_currency: 1.5909780425425113,
      price_change_percentage_24h_in_currency: -9.762620768375912,
      price_change_percentage_7d_in_currency: -11.327938030398894,
      __v: 0
    }
  ]


 */