echo "--------------------- installing nodejs --------------------"
cd ~
curl -sL https://deb.nodesource.com/setup_16.x -o nodesource_setup.sh
sudo bash nodesource_setup.sh
sudo apt install nodejs
node -v

echo "--------------------- installing mongodb --------------------"
curl -fsSL https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list
sudo apt update
sudo apt install mongodb-org
sudo systemctl start mongod.service
sudo systemctl status mongod
sudo systemctl enable mongod
sudo npm i -g pm2

exit


echo "---------------------- cloning repository -------------------"
cd ~
git clone https://github.com/Jazeb/trabic-game-backend.git
cd trabic-game-backend
npm i