const nodemailer = require('nodemailer');

const { MAILER_EMAIL, MAILER_PASSWORD } = process.env;

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: MAILER_EMAIL,
        pass: MAILER_PASSWORD
    }
});

const sendCodeToNo = user => {
    const body = `
        <h2 style="color:black;">Welcome to Trabic</h2>
        <p style="color:black;">Your login verification pin is ${user.code}</p> 
    `
    const mailOptions = {
        from: MAILER_EMAIL,
        to: user.email,
        subject: 'Trabic | verify pin',
        html: body
    };
    return new Promise((resolve, reject) => {
        transporter.sendMail(mailOptions, (err, response) => {
            if (err) return reject(err);
            return resolve(true)
        });
    });
}

module.exports = { sendCodeToNo }
