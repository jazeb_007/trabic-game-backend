const FCM = require('fcm-node');
const fcm_token = process.env.FCM_TOKEN;

const fcm = new FCM(fcm_token);

const sendNotification = (id, obj) => {
    return new Promise(async(resolve, reject) => {

        const model = {
            is_alert: obj.is_alert ? true : false,
            data: obj,
        }
        const message = {
            registration_ids: [id],
            collapse_key: 'FROM_ADMIN',
            // notification: {
            //     title: 'Price is updated',
            //     body: 'Price is increased'
            // },
            data: model,
            priority:'high'
        }
        fcm.send(message, (err, response) => {
            console.log(err || response)
            if (err) return reject(err);
            console.log(response);
        });
        return resolve(true);
    });
}

module.exports = { sendNotification };