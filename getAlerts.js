
const axios = require("axios");

const Device = require('./schema/Device');
const fcm = require('./fcm');

module.exports = async function getAlerts() {
    
    let alerts_data = await Device.find({}).lean();

    let crypto_units = ''
    let fiat_units = ''

    for (let alert of alerts_data) {
        alert = alert.alerts;
        for (let alrt of alert) {
            crypto_units += alrt['crypto_unit'] + ',';
            fiat_units += alrt['fiat_unit'] + ',';
        }
    };

    const check = {
        PERCENT: 1,
        RANGE: 2,
        PLUS_MINUS: 3
    }

    const gecko_url = `https://api.coingecko.com/api/v3/simple/price?ids=${crypto_units}&vs_currencies=${fiat_units}`;

    axios.get(gecko_url).then(response => {
        const data = response.data;

        for(let alrts of alerts_data){
            let alerts = alrts.alerts;

            for(let alert of alerts) {
                let fcm_token = alrts.fcm_token ? true : false;
                let repeat = alert.repeat;
    
                let { type, fiat_unit, crypto_unit } = alert;
                
                let curr_value = data[crypto_unit][fiat_unit];
                alert['curr_value'] = curr_value;
                alert['is_alert'] = true;

                if(!fcm_token) return
                
                if (type == check.RANGE) {
                    let { lt, gt } = alert;
                    if (!lt || !gt) return console.log("less than or greater values not exist");
            
                    // check if price exceeds the given amount
        
                    if (curr_value > gt || curr_value < lt) {
                        if(repeat) fcm.sendNotification(alrts.fcm_token, alert);
                        else if(!repeat && !alert.is_sent) {
                            fcm.sendNotification(alrts.fcm_token, alert);
                            
                            Device.findOneAndUpdate(
                                { _id:alrts._id, "alerts._id": alert._id },
                                { $set: { "alerts.$.is_sent": true }})
                                .then(status => console.log(status))
                                .catch(err => console.error(err));
                        } 
                    } 
                }

                else if (type == check.PERCENT) {
                    let { plus, minus, checkpoint } = alert; // plus, minus is %
                    if (!plus || !minus || !checkpoint) return console.log('Data is missing for PERCENT');
            
                    let plus_percent = plus / 100;
                    let minus_percent = minus / 100;
    
                    let checkpoint_plus = checkpoint + (checkpoint * plus_percent);
                    let checkpoint_minus = checkpoint - (checkpoint * minus_percent);
    
                    if (curr_value > checkpoint_plus || curr_value < checkpoint_minus) {
                        
                        if(repeat) fcm.sendNotification(alrts.fcm_token, alert);
                        else if(!repeat && !alert.is_sent){
                            fcm.sendNotification(alrts.fcm_token, alert);
                            Device.findOneAndUpdate(
                                { _id:alrts._id, "alerts._id": alert._id },
                                { $set: { "alerts.$.is_sent": true }})
                                .then(status => console.log(status))
                                .catch(err => console.error(err));
                        } 
                        // repeat && fcm_token && fcm.sendNotification(alrts.fcm_token, alert);

                    } 
                }

                else if (type == check.PLUS_MINUS) {

                    let { checkpoint, plus, minus } = alert;
            
                    if (!checkpoint || !plus || !minus) return console.log('Data is missing in PLUS_MINUS');
            
                    let checkpoint_plus = checkpoint + plus++;
                    let checkpoint_minus = checkpoint - plus++;
    
                    if (curr_value > checkpoint_plus || curr_value < checkpoint_minus) {

                        if(repeat) fcm.sendNotification(alrts.fcm_token, alert);
                        else if(!repeat && !alert.is_sent) {
                            fcm.sendNotification(alrts.fcm_token, alert);
                            Device.findOneAndUpdate(
                                { _id:alrts._id, "alerts._id": alert._id },
                                { $set: { "alerts.$.is_sent": true }})
                                .then(status => console.log(status))
                                .catch(err => console.error(err));
                        } 
                        // repeat && fcm_token && fcm.sendNotification(alrts.fcm_token, alert);
                        // console.log('condition is true');
                    }
                }
            }
        }

    }).catch(err => console.log(err));
}