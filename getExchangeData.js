
module.exports =  {
    init: async _ => {
        try {
            let axios = require("axios");
            let ExchangeData = require('./schema/ExchangeData');
        
            console.log('Fetching exchangeData');
        
            let gecko_url = 'https://v6.exchangerate-api.com/v6/cc330d0b8fca2564424e920b/latest/USD';
            
            const { data } = await axios.get(gecko_url);
        
            await ExchangeData.insertMany(data);

            return;
        
        } catch (error) {
            return console.log('Error in getching exchange data', error.message);
        }
    },
    
}

