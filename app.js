const { body, validationResult } = require('express-validator');
const cors = require("cors");
const bcrypt = require("bcrypt");
const express = require('express');
const formData = require("express-form-data");
const os = require("os");
const app = express();

const CodeVerification = require('./schema/CodeVerification');
const User = require('./schema/User');
const mailer = require('./mailer');
const Device = require('./schema/Device');
const ExchangeData = require('./schema/ExchangeData');
const CoinsModal = require('./schema/PricesModel');

const getExchangeData = require('./getExchangeData');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

const options = {
    uploadDir: os.tmpdir(),
    autoClean: true
};

app.use(formData.parse(options));
// delete from the request all empty files (size == 0)
app.use(formData.format());
// change the file objects to fs.ReadStream 
// app.use(formData.stream());
// union the body and the files
app.use(formData.union());

const generateNumber = _ => Math.floor(Math.random() * 1000) + 999;


app.post('/api/user/updateFcm', (req, res) => {
    const { fcm_token, _id } = req.body;
    if (!fcm_token || !_id)
        return res.status(400).json({ error: true, msg: 'Provide required fields' });

    User.findByIdAndUpdate(
        { _id },
        { $set: { fcm_token } },
        { new: true })
        .then(result => {
            console.log(result);
            return res.status(200).json({ error: false, msg: 'Token updated', data: result });
        });
});

/*
app.put('/api/updateDeviceId', (req, res) => {
    const { fcm_token, device_id } = req.body;
    if(!fcm_token || !device_id)
        return res.status(400).json({error: true, msg: 'Provide required fields'});
    
    const device = await User.findOne({ device_id });
    if(device) {
        User.findOneAndUpdate(
            { device_id }, 
            { $set:{ fcm_token }}, 
            { new: true })
            .then(result => res.status(200).json({error:false, msg: 'Token updated', data:result }));
    } else {
        User.create( 
            { fcm_token }
            )
            .then(result => {
            console.log(result);
            return res.status(200).json({error:false, msg: 'Token updated', data:result});
        });
    }
});
*/

app.post('/api/verifyCode', async (req, res) => {
    const { phone_no, code } = req.body;
    if (!code || !phone_no) return res.status(400).json({ error: true, msg: 'Provide phone no and code' });

    const exists = await CodeVerification.findOne({ phone_no, code, is_verified: false });
    if (!exists || exists.code != code) return res.status(400).json({ error: true, msg: 'Invalid code provided' });

    await CodeVerification.updateOne({ _id: exists._id }, { $set: { is_verified: 1 } });

    const user = await User.findOne({ phone_no });
    return res.status(200).json({ error: false, data: user });
});


app.post('/api/resendCode', async (req, res) => {
    const { phone_no } = req.body;
    if (!phone_no) return res.status(400).json({ error: true, msg: 'Provide phone no' });

    const user = await User.findOne({ phone_no });

    const code = generateNumber();

    user.code = code;
    res.status(200).json({ error: false, msg: 'Code sent to user email' });
    await mailer.sendCodeToNo(user);
    await CodeVerification.create({ phone_no, code, is_verified: false });
    setTimeout(async _ => {
        await CodeVerification.updateOne({ phone_no, code, is_verified: false }, { $set: { is_verified: 1 } });
    }, 10 * 60 * 60);
    return
});


app.post('/api/user/signup', [
    // body('email').isEmail().withMessage('Please enter a valid email address'),
    body('password').isLength({ min: 4, max: 10 }).withMessage('Password must contain min. 4 characters and max. 10 characters')
], async (req, res) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) return res.status(400).json({ error: true, msg: errors.array()[0].msg });

    if (!req.body.email || !req.body.phone_no)
        return res.status(400).json({ error: true, msg: 'Provide required fields' });

    const exists_by_email = await User.findOne({ email: req.body.email });
    if (exists_by_email) return res.status(400).json({ error: true, msg: 'Email already exists!' });

    const exists_by_phone = await User.findOne({ phone_no: req.body.phone_no });
    if (exists_by_phone) return res.status(400).json({ error: true, msg: 'Phone No already exists!' });

    req.body.total_balance = 0;
    req.body.sent_transactions = [
        {
            date: null,
            amount: 0,
            coins: 0,
            transaction_type: null
        }
    ];
    req.body.recieved_transactions = [
        {
            date: null,
            amount: 0,
            coins: 0,
            transaction_type: null
        }
    ];

    const salt = bcrypt.genSaltSync(10);
    req.body.password = bcrypt.hashSync(req.body.password, salt);

    const user = new User(req.body);
    user.save((err, result) => {
        if (err) {
            console.log(err);
            return res.status(400).json({ error: true, msg: 'Error user signup' })
        }
        console.log(result)
        return res.status(200).json({ error: false, msg: 'User created successfully', user: result });
    });
});

app.post('/api/user/login', async (req, res) => {
    const { phone_no, password } = req.body;
    if (!phone_no || !password)
        return res.json({ error: true, msg: 'Provide required fields' });

    const user = await User.findOne({ phone_no });
    if (!user) return res.status(400).json({ error: true, msg: 'User does not exist' });

    const isValid = await bcrypt.compare(password, user.password);
    if (!isValid) return res.status(400).json({ error: true, msg: 'Invalid password' });

    const code = generateNumber();

    user.code = code;

    res.status(200).json({ error: false, data: user });
    await mailer.sendCodeToNo(user);
    await CodeVerification.create({ phone_no: user.phone_no, code, is_verified: false });
    setTimeout(async _ => {
        await CodeVerification.updateOne({ email: user.email, code, is_verified: false }, { $set: { is_verified: 1 } });
    }, 1 * 60 * 60);
});

const check = {
    PERCENT: 1,
    RANGE: 2,
    PLUS_MINUS: 3
}

app.post('/api/currency/check', async (req, res) => {

    let { crypto_unit, type, fiat_unit, repeat, device_id } = req.body;


    if (!type || !crypto_unit || !fiat_unit || !device_id)
        return res.status(400).json({ error: true, msg: 'Provide required fields' });

    const alert_obj = {
        repeat,
        crypto_unit,
        device_id,
        fiat_unit,
        is_on: true,
        type: check[type]
    };
    // const TIME = 5 * 1000; // 5ms

    const device = await Device.findOne({ device_id }, { fcm_token: 1 });
    if (!device || !device.fcm_token) return res.status(400).json({ error: true, msg: 'FCM Token or device does not exist for this user' });

    if (type == 'RANGE') {
        const { lt, gt } = req.body;

        if (!lt || !gt) return res.status(400).json({ error: true, msg: 'Provide required fields of RANGE' });
        alert_obj['lt'] = lt;
        alert_obj['gt'] = gt;
    }
    else if (type == 'PERCENT') {

        let { plus, minus, checkpoint } = req.body; // plus, minus is %

        if (!plus || !minus || !checkpoint) return res.status(400).json({ error: true, msg: 'Provide required fields' });

        alert_obj['plus'] = plus;
        alert_obj['minus'] = minus;
        alert_obj['checkpoint'] = checkpoint;

    }
    else if (type == 'PLUS_MINUS') {

        let { checkpoint, plus, minus } = req.body;

        if (!checkpoint || !plus || !minus) return res.status(400).json({ error: true, msg: 'Provide required fields' });

        alert_obj['minus'] = minus;
        alert_obj['plus'] = plus;
        alert_obj['checkpoint'] = checkpoint;

    }

    await Device.findOneAndUpdate({ device_id }, { $push: { alerts: alert_obj } });

    const alert = await Device.findOne({ device_id });
    return res.status(200).json({ error: false, data: alert });

});

app.post('/api/device/register', (req, res) => {
    const { device_id } = req.body;
    if (!device_id) return res.status(400).json({ error: true, msg: 'Provide required fields' });

    Device.create(req.body)
        .then(device => res.status(200).json({ error: false, data: device }))
        .catch(err => res.status(400).json({ error: true, msg: err }));
});

app.post('/api/device/update', (req, res) => {
    const { device_id } = req.body;
    if (!device_id) return res.status(400).json({ error: true, msg: 'Provide required fields' });

    delete req.body.device_id;
    Device.findOneAndUpdate({ device_id }, req.body, { new: true })
        .then(device => res.status(200).json({ error: false, data: device }))
        .catch(err => res.status(400).json({ error: true, msg: err }));
});

app.delete('/api/device/delete/:device_id/:alert_id', (req, res) => {
    const { device_id, alert_id } = req.params;
    if (!device_id || !alert_id) return res.status(400).json({ error: true, msg: 'Provide required fields' });

    Device.findOneAndUpdate({ device_id }, { $pull: { alerts: { _id: alert_id } } })
        .then(_ => res.status(200).json({ error: false, msg: 'Alert deleted successfully' }))
        .catch(err => res.status(400).json({ error: true, msg: err }));
});

app.put('/api/device/updateFcm', (req, res) => {
    const { fcm_token, device_id } = req.body;
    if (!fcm_token || !device_id) return res.status(400).json({ error: true, msg: 'Provide required fields' });

    Device.findOneAndUpdate({ device_id }, { $set: { fcm_token } })
        .then(_ => res.status(200).json({ error: false, msg: 'Token updated' }))
        .catch(err => res.status(400).json({ error: true, msg: err }));
});

app.get('/api/getAllCoins', async (req, res) => {

    try {
        let exchangeData = await ExchangeData.findOne();
        let coinsPrices = await CoinsModal.find({});

        if(!exchangeData) {
            await getExchangeData.init();
            exchangeData = await ExchangeData.findOne();
        } 

        if(!coinsPrices) {
            await getExchangeData.init();
            exchangeData = await ExchangeData.findOne();
        } 


        const data = { coinsPrices, exchangeData }

        return res.status(200).json({ error: false, data });
        
    } catch (error) {
        console.log(error)
        return res.status(400).json({ error: true, msg: 'Something went wrong', error });
    }
});

app.put('/api/device/updateLogin', (req, res) => {
    const { device_id, flag } = req.body;
    if (!device_id) return res.status(400).json({ error: true, msg: 'Provide device_id of user or device' });

    Device.updateOne({ device_id }, { $set: { is_login: flag } })
        .then(_ => res.status(200).json({ error: false, msg: `loggedin is ${flag}` }))
        .catch(err => res.status(500).json({ error: true, msg: err }));
});


app.get('/api/coinInfo/:id', async (req, res) => {

    try {
        const { id } = req.params;
        if(!id) return res.status(400).json({ error:true, msg:'Provide id' });
        
        let coinsPrices = await CoinsModal.findOne({ id });

        return res.status(200).json({ error: false, data:coinsPrices });
        
    } catch (error) {
        console.log(error)
        return res.status(400).json({ error: true, msg: 'Something went wrong', error });
    }
});

app.get("/", (req, res) => res.status(200).json({ status: true, result: 'server is running' }));
app.all("*", (req, res) => res.status(404).json({ error: true, message: 'invalid url' }));

module.exports = app;