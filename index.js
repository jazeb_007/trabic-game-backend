require("dotenv").config();

const mongoose = require('mongoose');
const { Server } = require("socket.io");
const http = require('http');

const { MONGO_URI, PORT } = process.env;

const app = require('./app');

const server = http.createServer(app);

createSocketsConn();
connectMongo();
runServicesIntervals();


server.listen(PORT, _ => console.log(`server is running on port ${PORT}`));

async function connectMongo() {
    const mondoconfig = { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false }
    try {
        const _ = await mongoose.connect(MONGO_URI, mondoconfig);
        return console.log('MongoDB connected');
    } catch (err) {
        return console.log(err);
    }
}

function runServicesIntervals() {

    let getAlerts = require('./getAlerts');
    let getCoins = require('./getCoinsPrices');
    let getExchangeData = require('./getExchangeData');

    const TEN_SEC = 30000;
    const TWENTY_SEC = 20000;
    const TWENTYFOUR_HOURS = 1000 * 60 * 60 * 24;
    
    setInterval(_ => getCoins(), TEN_SEC);
    setInterval(_ => getAlerts(), TWENTY_SEC);
    setInterval(_ => getExchangeData.init(), TWENTYFOUR_HOURS); // 24 hours
    return
}

function createSocketsConn() {

    const ws = require('./websocket');
    
    const io = new Server(server);
    
    io.on("connection", socket => {
        console.log('\n\nNew Connection ', socket.id);
        
        // setInterval(_ => io.to(socket.id).emit('currencies', JSON.stringify({ name:'Jazeb', status:'Developer' })), 1000);
        
        ws.init(io, socket);

        socket.on('disconnect', reason => {
            console.log('#### ', reason, socket.id, ' ####');
            ws.removeSocketId(socket.id);
        });
    });
    
}