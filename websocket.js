
let connected = [];
let ws;
let io;

module.exports = {

    getConnectedIds: _ => connected,

    init: (ioClass, socket) => {
        
        console.log('\nInitializing new conection socket', socket.id)
        
        ws = socket;
        io = ioClass;
        
        if(!socket) throw new Error('Socket not initialized');

        socket.send("Connected to server ");

        console.log("socket assigned to WS", ws.id, '\n');

        connected.push(socket.id);

        return;
    },
    getWS: _ => {
        if (!ws) return console.log('Not Assigned socket')
        return ws;
    },

    getio: _ => {
        if (!io) return console.log('io not found')
        return io;
    },

    removeSocketId: socket_id => {
        console.log('DELETING', socket_id);
        connected = connected.filter(soc => soc !== socket_id);
    }
}; 
