const mongoose = require('mongoose').set('debug', true);
const Schema = mongoose.Schema;

const CodeVerification = Schema({
    code: {
        type: Number,
        required: true,
    },
    phone_no: {
        type: String,
        required: true,
    },
    is_verified: {
        type: Boolean,
        required: true,
    }
    
}, { collection: 'codeVerification' }, { __v: false });

module.exports = mongoose.model('codeVerification', CodeVerification);