const mongoose = require('mongoose').set('debug', true);
const Schema = mongoose.Schema;

const Device = Schema({
    device_id: {
        type: String,
        required: true,
        unique: true
    },
    fcm_token: {
        type: String,
        required: false
    },
    is_login: {
        type: Boolean
    },
    alerts: [
        {
            crypto_unit:{ 
                type: String
            },
            current_price:{ 
                type: String
            },
            fiat_unit:{ 
                type: String
            },
            type:{ 
                type: String
            },
            lt:{ 
                type: Number
            },
            gt:{ 
                type: Number
            },
            plus:{ 
                type: Number
            },
            minus:{ 
                type: Number
            },
            is_on:{
                type: Boolean,
                required: true,
                default: true
            },
            checkpoint:{
                type: Number
            },
            is_sent:{
                type: Boolean
            },
            repeat:{
                type: Boolean,
                required: true
            }
        }
    ]
}, { collection: 'device' }, { __v: false });

module.exports = mongoose.model('device', Device);
