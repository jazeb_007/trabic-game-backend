const mongoose = require('mongoose').set('debug', true);
const Schema = mongoose.Schema;

const User = Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    phone_no: {
        type: String,
        required: false,
        unique: true
    },
    total_balance: {
        type: Number,
        required: true
    },
    sent_transactions:[
        {
            date:{
                type: Date,
            },
            amount:{
                type: Number,
                default: 0
            },
            coins:{
                type: Number,
                default: 0
            },
            transaction_type:{
                type: String,
            }
        }
    ],
    recieved_transactions:[
        {
            date:{
                type: Date,
            },
            amount:{
                type: Number,
                default: 0
            },
            coins:{
                type: Number,
                default: 0
            },
            transaction_type:{
                type: String,
            }
        }
    ],
    transaction_code:{
        type: String,
        required: false
    }
}, { collection: 'user' }, { __v: false });

module.exports = mongoose.model('user', User);